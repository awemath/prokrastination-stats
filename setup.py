#!/usr/bin/env python3

import argparse
import os
import pwd

parser = argparse.ArgumentParser()
parser.add_argument('--project_dir',          default = "/home/prokrastination-stats",
                                            type    = str,
                                            help    = 'Directory with project.')

opt = parser.parse_args()
file_name = opt.project_dir + "/daemon-configuration/prokrastionation-stats.service"
user_name = pwd.getpwuid(os.getuid())[0]

os.system("pip install -r requirements.txt")

with open(file_name, 'r') as file:
    lines = file.readlines()

with open(file_name, 'w') as file:
    for i in range(len(lines)):
        lines[i] = lines[i].replace('path_to_project', opt.project_dir)
        lines[i] = lines[i].replace('username', user_name)
        file.write(lines[i])

install_xdtool = "apt-get install xdotool"
os.system(install_xdtool)

copy = "cp " + file_name + " /etc/systemd/system/"
os.system(copy)

os.system("systemctl daemon-reload")
os.system("systemctl enable prokrastionation-stats.service")
os.system("systemctl start prokrastionation-stats.service")
os.system("systemctl status prokrastionation-stats.service")
