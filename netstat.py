#!/usr/bin/env python3
import psutil
import socket
from publicsuffix import PublicSuffixList
from publicsuffix import fetch
from ipwhois import IPWhois


class NetStatStatisticGenerator():
    def __init__(self):
        psl_file = fetch()
        self.psl = PublicSuffixList(psl_file)

    def generate_stat(self):
        asn_dict = {}
        for connection in psutil.net_connections():
            addr = connection.raddr
            if addr:
                try:
                    host = socket.gethostbyaddr(addr.ip)
                except Exception:
                    continue
                if host:
                    obj = IPWhois(addr.ip)
                    res = obj.lookup_rdap()
                    asn_dict[res['asn']] = self.psl.get_public_suffix(host[0])
        result = []
        for key, value in asn_dict.items():
            result.append({'process_name': value})
        return result
