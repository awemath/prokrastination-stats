class ChromeFilter():
    def __init__(self, list):
        self.list = list

    def filter(self):
        for process in self.list:
            text = process['process_name']
            if text.find('Google Chrome') != -1:
                self.list.remove(process)

        return self.list
