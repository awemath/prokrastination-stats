var interval;

function startLogger() {
    if(interval) {
        return;
    }
    interval = window.setInterval(function () {
        var href = window.location.href;
        var xhttp = new XMLHttpRequest();
        xhttp.open("GET", "https://localhost:9909?href=" + href.toString(), true);
        xhttp.send()
    }, 1000);
}

function stopLogger() {
    if (interval) {
        window.clearInterval(interval);
        interval = undefined
    }
}

startLogger();

window.addEventListener('focus', startLogger);
window.addEventListener('blur', stopLogger);