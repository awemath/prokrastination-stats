from http.server import HTTPServer, BaseHTTPRequestHandler
from threading import Thread
from urllib.parse import urlparse
import json
import ssl

class BrowserCollector():
    def __init__(self, port, cert='', keyfile=''):
        self.last_urls = []
        this = self

        class MyHandler(BaseHTTPRequestHandler):
            def do_GET(self):
                self.send_response(200)
                self.end_headers()
                parsed_path = "".join(urlparse(self.path).query.split('=')[1:])
                this.last_urls.append(parsed_path)
                self.wfile.write(b'OK')

        server_address = ('', port)
        httpd = HTTPServer(server_address, MyHandler)
        if cert and keyfile:
            httpd.socket = ssl.wrap_socket(httpd.socket, certfile=cert, keyfile=keyfile, server_side=True)
        thread = Thread(target=httpd.serve_forever)
        thread.start()

    def name(self):
        return "Browsers"

    def generate_stat(self):
        urls = [{'process_name': "{uri.netloc}".format(uri = urlparse(url))} for url in set(self.last_urls)]

        self.last_urls = []
        return urls
