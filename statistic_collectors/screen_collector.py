import subprocess
import time
import re
import json
import argparse


class ScreenStatisticGenerator():
    def __init__(self, white_list_path, username):
        if white_list_path:
            with open(white_list_path, 'r') as f:
                config = json.load(f)
            self.white_list = [p['process_name'] for p in config['white_list']]
        else:
            self.white_list = []
        self.username = username

    def name(self):
        return "Screens"

    def application(self, s):
        slash = s.split('/')
        len_slash = len(slash)
        slash = slash[-1].strip()
        defis = ''.join(slash)
        check_def = defis.split(' - ')
        len_def = len(check_def)
        k = False
        if len_slash == 1 and len_def == 1 and check_def != ['']:
            first_word = re.findall(r'\w+', defis)[0]
            for app in self.white_list:
                if app == first_word:
                    return(first_word)
        check_def = check_def[-1]
        if (len_slash > 1 and len_def > 1) or (len_def > 1):
            # print(o)
            return(check_def)
        return []

    def google_chrome(self, s):
        slash = s.split('/')
        slash = slash[len(slash)-1]
        defis = ''.join(slash)
        o = defis.split('-')
        result = ''.join(o)
        o = o[-1].strip()
        if o == "Google Chrome":
            return(s.strip())
        return []

    def generate_stat(self):
        command = "export DISPLAY=':0.0'; export XAUTHORITY=/home/{}/.Xauthority; xdotool getwindowfocus getwindowname"\
            .format(self.username)
        screen_sub = subprocess.Popen(
            command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, shell=True)
        output, _ = screen_sub.communicate()
        screen = output.decode("UTF-8")
        app = self.application(screen)
        goo = self.google_chrome(screen)
        # return goo and app and [{'process_name': goo, 'from': 'screen'}]
        if goo != []:
            return [{'process_name': goo, 'from': 'screen'}]
        if app != []:
            return [{'process_name': app, 'from': 'screen'}]
        return []
# a = ScreenStatisticGenerator()
# while True:
#     print(a.generate_stat())
#     time.sleep(2)
