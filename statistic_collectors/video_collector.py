#!/usr/bin/env python3

#import os
import time
import psutil


class VideoStatisticGenerator():
    def __init__(self):
        pass

    def name(self):
        return "Video players"

    def generate_stat(self):
        """
        Если смотрим видео, то выдаем то, где смотрим
        """
        petr = [p.info for p in psutil.process_iter(attrs=['name', 'pid', 'username', 'status','open_files','memory_percent']) if p.info['memory_percent'] > 2]
        result = []
        for item in petr:
            t = 0
            if item['name'] == "totem":
                if self.posit_totem != item['open_files'][0][2]:
                    result.append[{'process_name': "Video Player", 'from' : "Video"}]
                    self.posit_totem = item['open_files'][0][2]
                    t = 1
            if item['name'] == "vlc":
                if self.posit_VLC != item['open_files'][0][2]:
                    result.append[{'process_name': "VLC", 'from' : "Video"}]
                    self.posit_VLC = item['open_files'][0][2]
                    t = 1
            if t == 1:
                break
        return result
