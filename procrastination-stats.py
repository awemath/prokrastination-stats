#!/usr/bin/env python3

from tkinter import *
from GUI.StatFrame import Shower
import json
import argparse

parser = argparse.ArgumentParser()
parser.add_argument( '--config_path',       default = "gui-config.json",
                                            type    = str,
                                            help    = 'Path to config file.' )

opt = parser.parse_args()

with open(opt.config_path) as f:
    config = json.load(f)


name = "Procrastination stats"
root = Tk(className=name)
root.title("Procrastination stats")

s = Shower(config, root)

s.update_idletasks()
root.minsize(s.winfo_reqwidth(), 460)
root.resizable(0, 0)

s.pack(side=LEFT, fill=Y)
root.mainloop()
s.save_config()
