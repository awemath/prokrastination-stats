import datetime
import argparse
from os.path import isfile

class DataReader():
    def __init__(self, config):
        self.calculated_data = {}
        self.group_config = config['groups'] if 'groups' in config else []

    def get_group(self, name):
        res = "black"
        for group in self.group_config:
            for item in group['items']:
                if item['name'] in name:
                    return group['color']
        return res

    def get_color(self, name):
        res = "black"
        for group in self.group_config:
            if group['name'] == name:
                return group['color']
        return res


    def calculate_raw(self, raw, dct, groups):
        groups_info = self.group_config
        data = raw.split(',')
        name = data[0]
        time = int(data[2])
        if name in dct.keys():
            dct[name] += time
        else:
            dct[name] = time
        notingroup = True
        for group in groups_info:
            for item in group['items']:
                if item['name'] in name:
                    groups[group['name']] = (groups[group['name']] if group['name'] in groups else 0) + \
                        time
                    notingroup = False
        if notingroup:
            groups['notgrouped'] = (groups['notgrouped'] if 'notgrouped' in groups else 0) + time
        return dct, groups

    def sum_dicts(self, dict1, dict2):
        dct = dict2
        for key in dict1:
            if key in dct.keys():
                dct[key] += dict1[key]
            else:
                dct[key] = dict1[key]
        return dct

    def calculate_full_files(self, pathes):
        output_dct, output_groups = {}, {}
        for path in pathes:
            dct, groups = self.calculate_full_file(path)
            output_dct = self.sum_dicts(dct, output_dct)
            output_groups = self.sum_dicts(groups, output_groups)
        return output_dct, output_groups

    def calculate_full_file(self, path):
        if path in self.calculated_data:
            return self.calculated_data[path]

        if not isfile(path):
            return {},{}
        #print(path)
        dct = {}
        groups = {}

        with open(path) as day_file:
            for raw in day_file:
                dct, groups = self.calculate_raw(raw, dct, groups)
        self.calculated_data[path] = (dct, groups)
        return dct, groups

    def calculate_file_part(self, path, start_timestamp, end_timestamp):
        dct = {}
        groups = {}
        with open(path) as day_file:
            for raw in day_file:
                data = raw.split(',')
                if start_timestamp <= int(data[2]) <= end_timestamp:
                    dct, groups = self.calculate_raw(raw, dct, groups)
        return dct, groups

class DataPreparator():
    def __init__(self, config):
        self.config = config

    def get_week_days(self):
        file_dir = self.config['file_dir']
        output_list = [datetime.datetime.now() - datetime.timedelta(i) for i in range(7)]
        output_list = ["{}/{}".format(file_dir, time.strftime("%Y-%m-%d")) for time in output_list]
        return output_list

    def prepare_information(self, result):
        output = []
        result = result[0]
        result_func = lambda x: int(round(result[x] / 60))
        output = [(key, result_func(key)) for key in result if result_func(key)]
        time_array = [time for _, time in output]
        #print(time_array)
        max_time = max(time_array) if time_array else 0
        output.sort(reverse=True, key=lambda x: x[1])
        #print((output[:head], max_time))
        return (output, max_time)

    def prepare_group_information(self, result):
        output = []
        result = result[1]
        result_func = lambda x: int(round(result[x] / 60))
        output = [(key, result_func(key)) for key in result if result_func(key)]
        time_array = [time for _, time in output]
        # print(time_array)
        max_time = sum(time_array) if time_array else 0
        output.sort(reverse=True, key=lambda x: x[1])
        return (output, max_time)

    def get_today(self):
        file_dir = self.config['file_dir']
        now = datetime.datetime.now()
        return "{}/{}".format(file_dir, now.strftime("%Y-%m-%d"))
