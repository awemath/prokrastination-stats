#!/usr/bin/env bash

name=$(sed -e 's/\.//g' <<< "$1")

wget -q --timeout=5 -O "icons/$name.ico" "$1/favicon.ico"
convert "icons/$name.ico" -resize 100X100 -quality 100 -quiet "icons/$name.png"
rm "icons/$name.ico"
i=0
while [[ -f "icons/$name-$i.png" ]]
do
mv "icons/$name-$i.png" "icons/$name.png"
i=$((i+1))
done
