from tkinter import *
from tkinter.ttk import Progressbar, Style
from tkinter.font import Font, BOLD, families
from GUI.VSFrame import VerticalScrolledFrame
from GUI.DataParser import DataReader, DataPreparator
from math import ceil, floor
from threading import Thread
import os
import datetime
import subprocess
import json


def timestring(time):
    VAL_LST = ['min', 'h', 'd']
    TIME_INT = [60, 24, 24]
    i = 0
    res = ""
    while (i < 3) and (time > 0):
        if i == 2:
            res = str(time) + " " + VAL_LST[i] + " " + res
        else:
            res = str(time % TIME_INT[i]) + " " + VAL_LST[i] + " " + res
        time = time // TIME_INT[i]
        i += 1
    return res


class StatFrame(Frame):
    def __init__(self, root, image_reader, name, time, MAX_TIME, color):
        self.color = color
        BGC = 'azure'
        Frame.__init__(self, root, background=BGC, highlightbackground="black", highlightthickness=1)
        self.name = name
        self.panel = Frame(self,  background=BGC)
        self.top_panel = Frame(self.panel, background=BGC)
        myfont = Font(family='bitstream charter', size=18, weight=BOLD)
        self.stat_name = Label(self.top_panel,
                               text=name,
                               font=myfont,
                               background=BGC)
        MAX_LAB = Label(text=timestring(MAX_TIME))
        self.abs_time = Label(self.top_panel,
                              text=timestring(time),
                              font=myfont,
                              background=BGC)
        s = Style()
        s.theme_use('clam')
        theme = self.color + ".Horizontal.TProgressbar"
        s.configure(theme,
                    background=self.color,
                    troughcolor=BGC,
                    foreground=self.color,
                    bordercolor=BGC)
        self.stat_val = Progressbar(self.panel,
                                    style=theme,
                                    orient="horizontal",
                                    length=800,
                                    mode="determinate")
        if time * 800 < 9 * MAX_TIME:
            self.stat_val["value"] = 9
            self.stat_val["maximum"] = 800
        else:
            self.stat_val["maximum"] = MAX_TIME
            self.stat_val["value"] = time

        self.img = image_reader.image_label(self, 50, 50, image_reader.get_image_path(name), bg=BGC)
        self.stat_name.pack(side=LEFT, anchor=W)
        self.abs_time.pack(side=RIGHT, anchor=E)
        self.top_panel.pack(side=TOP, fill=X, anchor=NE)
        self.stat_val.pack(side=BOTTOM, anchor=SE)
        self.panel.pack(side=RIGHT, fill=X, anchor=E)
        self.img.pack(side=LEFT, anchor=W)
        #self.stat_name.grid(row=0, column=0)
        #self.abs_time.grid(row=0, column=1)
        #self.stat_val.grid(row=1)
        self.bind("<Button>", self.mouseclick)
        #self.stat_val.bind("<Button>", self.mouseclick)
        #self.stat_name.bind("<Button>", self.mouseclick)
        #self.abs_time.bind("<Button>", self.mouseclick)

    def mouseclick(self, event):
        pass
        #print("click " + self.name)


class StatShower(VerticalScrolledFrame):
    def __init__(self, root, config, lst=[], max_time=0):
        self.COLOR = ['green', 'blue', 'red']
        VerticalScrolledFrame.__init__(self, root)
        self.root = root
        self.stat_list = []
        self.lst = lst
        self.config = config
        self.max_time = max_time
        self.initialized = 0

    def initialize_first(self, n=5):
        self.initialized = n
        def create_stat_frame(item):
            color = DataReader(self.config).get_group(item[0])
            sf = StatFrame(self.interior, self.root.image_reader, item[0], item[1], self.max_time, color)
            sf.pack(pady=5, ipady=7)
        s = [create_stat_frame(item) for item in self.lst[:n]]

    def initialize(self):
        #print(datetime.datetime.now())
        n = self.initialized
        def create_stat_frame(item):
            color = DataReader(self.config).get_group(item[0])
            sf = StatFrame(self.interior, self.root.image_reader, item[0], item[1], self.max_time, color)
            sf.pack(pady=5, ipady=7)
        s = [create_stat_frame(item) for item in self.lst[n:]]
        # for item in self.lst:
        #     color = DataReader(self.config).get_group(item[0])
        #     sf = StatFrame(self.interior, self.root.image_reader, item[0], item[1], self.max_time, color)
        #     sf.pack(pady=5, ipady=7)
        #print(datetime.datetime.now())


class Shower(Frame):
    def __init__(self, config, root, *args, **kw):
        self.config = config
        self.data_parser = DataReader(config)
        self.data_preparator = DataPreparator(config)
        self.buffer = {}
        Frame.__init__(self, root, *args, **kw)
        self.image_reader = ImageAdditor(config)
        if 'day' not in self.buffer:
            day = self.data_parser.calculate_full_file(self.data_preparator.get_today())

            day_group = self.data_preparator.prepare_group_information(day)
            day = self.data_preparator.prepare_information(day)
            if not day[0]:
                day = [[('no day info', 0)], 1]
            statshow = StatShower(self, config, day[0], day[1])

            statshow.initialize_first()

            groupshow = GroupPanel(self, config, day_group[0], day_group[1], 'white')
            self.buffer['day'] = statshow
            self.buffer['daygroup'] = groupshow
        else:
            statshow = self.buffer['day']
            groupshow = self.buffer['daygroup']

        init = Thread(target = self.init_week)
        init.start()

        self.group_show = groupshow
        self.statshow = statshow
        self.bpanel = Frame(self, height=100)
        myfont = Font(family='bitstream charter', size=25, weight=BOLD)
        self.day = Label(self.bpanel,
                          text="Day",
                          font=myfont,
                          background='white')
        self.week = Label(self.bpanel,
                           text="Week",
                           font=myfont,
                           background='light gray')
        #self.day.bind("<Button>", self.show_day)
        self.week.bind("<Button>", self.show_week)
        self.day.pack(side=LEFT, fill=X, expand=1)
        self.week.pack(side=RIGHT, fill=X, expand=1)
        self.bpanel.pack(side=TOP, fill=X)
        self.group_show.pack(side=TOP, fill=X)
        self.statshow.pack(side=BOTTOM, expand=1, fill=Y)
        init = Thread(target = statshow.initialize)
        init.start()


    def init_week(self):
        if 'week' not in self.buffer:
            days = self.data_preparator.get_week_days()
            day = self.data_parser.calculate_full_files(days)
            day_group = self.data_preparator.prepare_group_information(day)
            day = self.data_preparator.prepare_information(day)
            statshow = StatShower(self, self.config, day[0], day[1])
            # init = Thread(target = statshow.initialize)
            # init.start()
            groupshow = GroupPanel(self, self.config, day_group[0], day_group[1], 'white')
            self.buffer['week'] = statshow
            self.buffer['weekgroup'] = groupshow
            statshow.initialize()
        else:
            statshow = self.buffer['week']
            groupshow = self.buffer['weekgroup']

    def show_day(self, event):
        if 'day' not in self.buffer:
            day = self.data_parser.calculate_full_file(self.data_preparator.get_today())
            day_group = self.data_preparator.prepare_group_information(day)
            day = self.data_preparator.prepare_information(day)
            statshow = StatShower(self, self.config, day[0], day[1])
            # init = Thread(target = statshow.initialize)
            # init.start()
            self.buffer['day'] = statshow
            groupshow = GroupPanel(self, self.config, day_group[0], day_group[1], 'white')
            self.buffer['daygroup'] = groupshow
            statshow.initialize()
        else:
            statshow = self.buffer['day']
            groupshow = self.buffer['daygroup']

        #self.day_result = statshow
        self.statshow.forget()
        self.statshow = statshow
        self.group_show.forget()
        self.group_show = groupshow
        self.group_show.pack(side=TOP, fill=X)
        self.statshow.pack(side=BOTTOM, expand=1, fill=Y)
        self.day.unbind("<Button>")
        self.week.bind("<Button>", self.show_week)
        self.day.config(background='white')
        self.week.config(background='light gray')
        #self.day.config(state=DISABLED)
        #self.week.config(state=NORMAL)

    def show_week(self, event):
        if 'week' not in self.buffer:
            days = self.data_preparator.get_week_days()
            day = self.data_parser.calculate_full_files(days)
            day_group = self.data_preparator.prepare_group_information(day)
            day = self.data_preparator.prepare_information(day)
            statshow = StatShower(self, self.config, day[0], day[1])
            self.buffer['week'] = statshow
            groupshow = GroupPanel(self, self.config, day_group[0], day_group[1], 'white')
            self.buffer['weekgroup'] = groupshow
        else:
            statshow = self.buffer['week']
            groupshow = self.buffer['weekgroup']


        #self.week_result = statshow
        self.statshow.forget()
        self.group_show.forget()
        self.statshow = statshow
        self.group_show = groupshow
        self.group_show.pack(side=TOP, fill=X)
        self.statshow.pack(side=BOTTOM, expand=1, fill=Y)
        self.week.unbind("<Button>")
        self.day.bind("<Button>", self.show_day)
        self.week.config(background='white')
        self.day.config(background='light gray')
        #self.week.config(state=DISABLED)
        #self.day.config(state=NORMAL)

    def save_config(self):
        self.image_reader.save_config()


class GroupPanel(Canvas):
    def __init__(self, root, config, lst=[], maxtime=0, bg='white'):
        dr = DataReader(config)
        root.update_idletasks()
        #width = root.winfo_reqwidth() - 10
        width = 857
        Canvas.__init__(self, root, background=bg, width=width+10, height=25)
        prev = 5
        for items in lst:
            next = prev + items[1] * width // maxtime
            color = dr.get_color(items[0])
            self.create_rectangle(prev, 5, next, 20,  fill=color, outline=color)
            #self.create_text(prev + 1, 11, text=items[0], font=('bitstream charter', 16), width=next - prev - 1, anchor=NW)
            prev = next


class ImageAdditor:
    def __init__(self, config):
        self.config = config
        self.image_buffer = {}
        pass

    def get_image_path(self, name):
        if 'icons' not in self.config:
            return "icons/default.png"
        for pattern in self.config['icons']:
            if pattern in name:
                return self.config['icons'][pattern]['path']

        command = "bash GUI/iconsaver.sh " + name
        screen_sub = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, shell=True)
        output, error = screen_sub.communicate()
        path = name.replace('.', '')
        if os.path.isfile("icons/" + path + ".png"):
            self.config['icons'][name] = {}
            self.config['icons'][name]['path'] = "icons/" + path + ".png"
            return self.config['icons'][name]['path']
        # if '.com' in name or '.ru' in name or '.org' in name or '.io' in name:
        #     if name not in os.listdir('icons'):
        #         try:
        #             path = '{}/{}'.format('icons', name)
        #             icon = download_favicon(name)
        #             with open(path, 'w+') as f:
        #                 f.write(icon)
        #             return path
        #         except:
        #             return "icons/default.png"
        #     else:
        #         return '{}/{}'.format('icons', name)
        return "icons/default.png"


    def image_label(self, root, height, width, path='icons/default.png', bg='white'):
        if path in self.image_buffer:
            img = self.image_buffer[path]
        if path not in self.image_buffer:
            #print(path)
            img = PhotoImage(file = path)
            img_h = img.height()
            img_w = img.width()
            sub = max(ceil(img_h / height), ceil(img_w / width))
            zoom = min(floor(height / img_h), floor(width / img_w))
            if zoom == 0:
                img = img.subsample(sub)
            else:
                img = img.zoom(zoom)

        res = Label(root, image=img, height=height, width=width, background=bg)
        res.image = img

        self.image_buffer[path] = img

        return res

    def save_config(self):
        with open('gui-config.json', 'w') as f:
            f.write(json.dumps(self.config))


if __name__ == "__main__":
    root = Tk()
    r = GroupPanel(root, [('1', 10), ('2', 20)], 30)
    r.pack()
    root.mainloop()
