class MockStatisticGenerator():
    def __init__(self):
        pass

    def generate_stat(self):
        return [
            {
                'process_name': "telegram"
            },
            {
                'process_name': "twitch.tv"
            },
            {
                'process_name': "twitter.com"
            }
        ]
