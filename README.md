# prokrastination-stats

## Установка плагина для Google Chrome:
В поисковой строке Google Chrome написать `chrome://extensions`. Нажать галочку "Developer mode" => "Load unpacked extension..". В открывшемся окне нужно выбрать папку chrome-plugin данного проект

## Запуск демона:
В файле daemon-configuration/prokrastionation-stats.service указать путь до скрипта daemon.py
```
apt-get install xdotool
cp daemon-configuration/prokrastionation-stats.service /etc/systemd/system/
systemctl daemon-reload
systemctl enable prokrastionation-stats.service
systemctl start prokrastionation-stats.service
systemctl status prokrastionation-stats.service
```

Демон и плагин общаются с помощью самоподписанного сертификата, который нужно подтвердить. Поэтому, для корректного сбора статистики необходимо после запуска демона перейти в браузере по адресу https://localhost:9909 и подтвердить переход, чтобы google chrome доверял данному сертификату.
Но google chrome после этого будет на каждой странице предупреждать о том, что подключение небезопасно.

## Запуск визуализатора:
Должен быть установлен tkinter для python3 (`sudo apt-get install python3-tk`). Выполнить `./procrastination-stats.py`.

