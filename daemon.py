#!/usr/bin/env python3

import os
import argparse
import time
import datetime
from statistic_filters.chrome_filter import ChromeFilter

from mock import MockStatisticGenerator
from statistic_collectors.video_collector import VideoStatisticGenerator
from statistic_collectors.browser_collector import BrowserCollector
from statistic_collectors.screen_collector import ScreenStatisticGenerator

from functools import partial

#   Arguments parsing.
parser = argparse.ArgumentParser()
parser.add_argument( '--file_dir',          default = "/var/log/activity_log",
                                            type    = str,
                                            help    = 'Directory for logs.' )
parser.add_argument( '--sec',               default = 5,
                                            type    = int,
                                            help    = 'Frequency of statistic generation.' )
parser.add_argument( '--port',              default = 9909,
                                            type    = int,
                                            help    = 'Port for browser collector.' )
parser.add_argument( '--cert_path',         default = None,
                                            type    = str,
                                            help    = 'Path to ssl certificate.' )
parser.add_argument( '--whitelist_path',    default = None,
                                            type    = str,
                                            help    = 'Path to whitelist applications certificate.' )
parser.add_argument( '--user_name',         default = None,
                                            type    = str,
                                            help    = 'User name.' )

opt = parser.parse_args()

if not os.path.exists(opt.file_dir):
    print("Directory was missing.")
    os.makedirs(opt.file_dir)
    print("Directory has been created")

cert_path = ""
keyfile = ""

if opt.cert_path:
    cert_path = "{}/{}".format(opt.cert_path, 'cert.pem')
    keyfile = "{}/{}".format(opt.cert_path, 'key.pem')

all_generators = [
    partial(ScreenStatisticGenerator, white_list_path = opt.whitelist_path, username=opt.user_name),
    partial(VideoStatisticGenerator),
    partial(BrowserCollector, port=opt.port, cert=cert_path, keyfile=keyfile)
]

statistic_generators = []

for generator in all_generators:
    # try:
    result = generator()
    # except:
    #     continue
    statistic_generators.append(result)
    print("{} generator has successfully loaded".format(result.name()))

while True:
    current_results = []

    current_timestamp = int(time.time())

    now = datetime.datetime.now()
    for statistic_generator in statistic_generators:
        current_results = current_results + statistic_generator.generate_stat()

    chrome_filter = ChromeFilter(current_results)
    current_results = chrome_filter.filter()

    statistic_results = ["{},{},{}\n".format(r['process_name'], current_timestamp, opt.sec) for r in current_results]
    current_file = "{}/{}".format(opt.file_dir, now.strftime("%Y-%m-%d"))

    with open(current_file, 'a+') as f:
        f.writelines(statistic_results)

    time.sleep(opt.sec)
